
import { useState } from "react";
import Expenses from "./components/Expenses/Expenses.js";
import NewExpense from "./components/NewExpense/NewExpense.js";
function App() {

  const [NewExpenses, setExpenses] = useState([{}])
  const expenses = [
    {
      id: "e1",
      date: new Date(2014,12,21),
      title: "Car Insurrance",
      ammount: 4335,
    },
    {
      id: "e2",
      date: new Date(2019,13,12),
      title: "Washing machine",
      ammount: 335,
    },
    {
      id: "e3",
      date: new Date(2022,11,20),
      title: "Bike",
      ammount: 4335,
    },
    {
      id: "e4",
      date: new Date(2022,13,6),
      title: "Car machine",
      ammount: 335,
    },
    {
      id: "e5",
      date: new Date(2022,13,7),
      title: "Walking machine",
      ammount: 335,
    },
    {
      id: "e6",
      date: new Date(2014,12,21),
      title: "Car Insurrance",
      ammount: 4335,
    },
    {
      id: "e7",
      date: new Date(2019,13,12),
      title: "Washing machine",
      ammount: 335,
    },
    {
      id: "e8",
      date: new Date(2022,11,7),
      title: "Bike",
      ammount: 4335,
    },
    {
      id: "e9",
      date: new Date(2022,13,7),
      title: "Car machine",
      ammount: 335,
    },
    {
      id: "e10s",
      date: new Date(2022,13,7),
      title: "Walking machine",
      ammount: 335,
    },
  ];

 const AddExpenseHandler = (data)=>{

    const expense ={
      ...data
    }

    console.log(expense)
    setExpenses(expense)
 }

  return (
    <div>
      <NewExpense onAddExpense= {AddExpenseHandler}/>
      <Expenses items={expenses} />

    </div>
  );
}

export default App;
