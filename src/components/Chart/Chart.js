import React from "react";
import "./Chart.css";
import ChartBar from "./ChartBar";
const Chart = (props) => {
  const dataPointValue = props.dataPoints.map(dataPoint=>dataPoint.value)
  console.log("data point from expense value"+dataPointValue)
  const totalMaximum = Math.max(...dataPointValue)

  console.log(totalMaximum)
  return (
    <div className="chart">
      {props.dataPoints.map(dataPoint =>
        <ChartBar
          key={dataPoint.label}
          value={dataPoint.value}
          maxValue={totalMaximum}
          label={dataPoint.label}
        />
      )}
    </div>
  );
};

export default Chart;
