import React, { useState } from "react";
import "./Expenses.css";
import Card from "../UI/Card";
import ExpensesFilter from "./ExpensesFilter";
import ExpensesList from "./ExpensesList";
import ExpensesChart from "./ExpensesChart";
const Expenses = (props) => {
  const [filterYear, setFilterYear] = useState("2020");
  const saveYearHandler = (yearValue) => {
    setFilterYear(yearValue);

  };

  console.log(props.items);

  const filteredExpenses = props.items.filter(expense => {
    return expense.date.getFullYear().toString() === filterYear;
  });
  console.log("git year"+filteredExpenses)
  return (
    <li>
      <Card className="expenses">
        <ExpensesFilter selected={filterYear} onFilterDate={saveYearHandler} />
        <ExpensesChart expenses={filteredExpenses}/>
        <ExpensesList items={filteredExpenses} />
      </Card>
    </li>
  );
};

export default Expenses;
