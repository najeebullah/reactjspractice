import React, { useState } from "react";
import './NewExpense.css'
import ExpenseForm from './ExpenseForm.js'

const NewExpense = (props) => {

  const [isEdited, setIsEdited] = useState(false);
  const cancelForm = ()=>{

    setIsEdited(false)
  }

  const saveExpenseDataHandler = (data)=>{

    const expenseData = {
      ...data,
      id: Math.random().toString()

      
    }
    cancelForm()

    props.onAddExpense(expenseData)
    console.log(expenseData) 

  }

  const startEdit = ()=>{

    setIsEdited(true)

  }
 
  
  return <div className="new-expense">
    
    {!isEdited?<button onClick={startEdit}>Add new expense</button> :
    <ExpenseForm onSaveExpenseData = {saveExpenseDataHandler} onCancel ={cancelForm}/>}
  </div>;
};
export default NewExpense;
